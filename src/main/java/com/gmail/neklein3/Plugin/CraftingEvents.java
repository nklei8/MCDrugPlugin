package com.gmail.neklein3.Plugin;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Objects;

public class CraftingEvents implements Listener {

    @EventHandler
    public void onCraftLSD(CraftItemEvent event) {
        ItemStack result = event.getInventory().getResult();
        ItemMeta resultMeta = result.getItemMeta();

        CraftingInventory craftingInventory = event.getInventory();

        if (resultMeta.hasDisplayName()) {
            System.out.println("has name");
            if (resultMeta.getDisplayName().equals(ChatColor.GRAY + "LSD")) {
                System.out.println("has name LSD");
                for (ItemStack item : craftingInventory.getMatrix()) {
                    System.out.println("for each");
                    if (item.hasItemMeta()) {
                        if (!(item.getItemMeta().getDisplayName().equals(ChatColor.GRAY + "LSD Extract"))) {
                            event.setCancelled(true);
                            System.out.println("has no lsd extract");
                            System.out.println("event cancelled true");
                        }
                    }
                }
            }
        }
    }
}
