package com.gmail.neklein3.Plugin;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class DrugPlugin extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
        getLogger().info("onEnable is being called");
        getLogger().info("attempting to initialize events");

        // register weed events
        this.getServer().getPluginManager().registerEvents(new Events(), this);

        //register cocaine events
        this.getServer().getPluginManager().registerEvents(new CocaineEvents(), this);

        //register LSD stuff
        this.getServer().getPluginManager().registerEvents(new LSDEvents(), this);
        this.getServer().getPluginManager().registerEvents(new CraftingEvents(), this); //crafting lsd event

        getLogger().info("initialized events");

        //LSD Recipe
        //custom item (product of recipe)
        ItemStack item = new ItemStack(Material.PAPER);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.GRAY + "LSD");
        item.setItemMeta(meta);
        //making the recipe
        NamespacedKey key = new NamespacedKey(this, "lsd");
        ShapedRecipe lsdRecipe = new ShapedRecipe(key, item);
        lsdRecipe.shape(" I ", "IPI", " I ");
        lsdRecipe.setIngredient('I', Material.IRON_NUGGET);
        lsdRecipe.setIngredient('P', Material.PAPER);
        //adding it
        Bukkit.addRecipe(lsdRecipe);
    }
    @Override
    public void onDisable() {
        getLogger().info("onDisable is called!");
        getLogger().info("BYE BYE!!!");
    }

}
