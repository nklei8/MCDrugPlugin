package com.gmail.neklein3.Plugin;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import java.util.ArrayList;
import java.util.List;

public class CocaineEvents implements Listener {

    @EventHandler
    public void blockBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        Player player = event.getPlayer();

        if (block.getType().equals(Material.SUGAR_CANE)) {
            ItemStack itemStack = new ItemStack(Material.SUGAR, 1);
            ItemMeta meta = itemStack.getItemMeta();
            List<String> lore = new ArrayList<>();

            meta.setDisplayName(ChatColor.GRAY + "Cocaine");

            lore.add(ChatColor.WHITE + "right click to snort");

            meta.setLore(lore);
            itemStack.setItemMeta(meta);
            player.getInventory().addItem(itemStack);

        }
    }

    @EventHandler
    public void onPlayerUseCocaine(PlayerInteractEvent event)  {
        Player player = event.getPlayer();
        Action action = event.getAction();
        ItemStack item = player.getInventory().getItemInMainHand();

        int originalItemAmount = item.getAmount();
        ItemStack itemOneLess = new ItemStack(Material.SUGAR, originalItemAmount - 1);
        ItemStack itemTwoLess = new ItemStack(Material.SUGAR, originalItemAmount - 2);
        ItemMeta meta = itemOneLess.getItemMeta();
        ItemMeta meta2 = itemTwoLess.getItemMeta();
        List<String> lore = new ArrayList<>();

        meta.setDisplayName(ChatColor.GRAY + "Cocaine");
        meta2.setDisplayName(ChatColor.GRAY + "Cocaine");

        lore.add(ChatColor.WHITE + "right click to snort");


        meta.setLore(lore);
        meta2.setLore(lore);
        itemOneLess.setItemMeta(meta);
        itemTwoLess.setItemMeta(meta);

        Location playerLocation = player.getLocation();

        if (action.equals(Action.RIGHT_CLICK_BLOCK)) {
            if (item.getType().equals(Material.SUGAR))  {
                if (item.getItemMeta().getDisplayName().equals(ChatColor.GRAY + "Cocaine")) {
                    player.removePotionEffect(PotionEffectType.SPEED);
                    player.removePotionEffect(PotionEffectType.JUMP);
                    player.removePotionEffect(PotionEffectType.SLOW);
                    player.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 600, 7));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 500, 1));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 400, 2));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 1000, 2));
                    player.getInventory().removeItem(item);
                    player.getInventory().addItem(itemOneLess);
                    player.spawnParticle(Particle.SNOW_SHOVEL, playerLocation, 100);
                }
            }
        } else if (action.equals(Action.RIGHT_CLICK_AIR)) {
            if (item.getType().equals(Material.SUGAR))  {
                if (item.getItemMeta().getDisplayName().equals(ChatColor.GRAY + "Cocaine")) {
                    player.removePotionEffect(PotionEffectType.SPEED);
                    player.removePotionEffect(PotionEffectType.JUMP);
                    player.removePotionEffect(PotionEffectType.SLOW);
                    player.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 600, 7));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 500, 1));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 400, 2));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 1000, 2));
                    player.getInventory().removeItem(item);
                    player.getInventory().addItem(itemTwoLess);
                    player.spawnParticle(Particle.SNOW_SHOVEL, playerLocation, 100);
                }
            }
        }
    }
}