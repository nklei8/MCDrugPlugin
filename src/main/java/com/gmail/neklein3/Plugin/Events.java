package com.gmail.neklein3.Plugin;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import java.util.ArrayList;
import java.util.List;

public class Events implements Listener {

    @EventHandler
    public void blockBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        Player player = event.getPlayer();

        if (block.getType().equals(Material.WHEAT)) {
            ItemStack itemStack = new ItemStack(Material.DRIED_KELP, 1);
            ItemMeta meta = itemStack.getItemMeta();
            List<String> lore = new ArrayList<>();

            meta.setDisplayName(ChatColor.DARK_GREEN + "Weed");

            lore.add(ChatColor.GREEN + "right click to smoke");

            meta.setLore(lore);
            itemStack.setItemMeta(meta);
            player.getInventory().addItem(itemStack);

        }
    }

    @EventHandler
    public void onPlayerUseWeed(PlayerInteractEvent event)  {
        Player player = event.getPlayer();
        Action action = event.getAction();
        ItemStack item = player.getInventory().getItemInMainHand();

        int originalItemAmount = item.getAmount();
        ItemStack itemOneLess = new ItemStack(Material.DRIED_KELP, originalItemAmount - 1);
        ItemStack itemTwoLess = new ItemStack(Material.DRIED_KELP, originalItemAmount - 2);
        ItemMeta meta = itemOneLess.getItemMeta();
        ItemMeta meta2 = itemTwoLess.getItemMeta();
        List<String> lore = new ArrayList<>();

        meta.setDisplayName(ChatColor.DARK_GREEN + "Weed");
        meta2.setDisplayName(ChatColor.DARK_GREEN + "Weed");

        lore.add(ChatColor.GREEN + "right click to smoke");


        meta.setLore(lore);
        meta2.setLore(lore);
        itemOneLess.setItemMeta(meta);
        itemTwoLess.setItemMeta(meta);

        Location playerLocation = player.getLocation();

        if (action.equals(Action.RIGHT_CLICK_BLOCK)) {
            if (item.getType().equals(Material.DRIED_KELP))  {
                if (item.getItemMeta().getDisplayName().equals(ChatColor.DARK_GREEN + "Weed")) {
                    player.removePotionEffect(PotionEffectType.SPEED);
                    player.removePotionEffect(PotionEffectType.JUMP);
                    player.removePotionEffect(PotionEffectType.SLOW);
                    player.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 600, 7));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 500, 1));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 400, 2));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 1000, 2));
                    player.getInventory().removeItem(item);
                    player.getInventory().addItem(itemOneLess);
                    player.spawnParticle(Particle.SMOKE_LARGE, playerLocation, 5);
                }
            }
        } else if (action.equals(Action.RIGHT_CLICK_AIR)) {
            if (item.getType().equals(Material.DRIED_KELP))  {
                if (item.getItemMeta().getDisplayName().equals(ChatColor.DARK_GREEN + "Weed")) {
                    player.removePotionEffect(PotionEffectType.SPEED);
                    player.removePotionEffect(PotionEffectType.JUMP);
                    player.removePotionEffect(PotionEffectType.SLOW);
                    player.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 600, 7));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 500, 1));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 400, 2));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 1000, 2));
                    player.getInventory().removeItem(item);
                    player.getInventory().addItem(itemTwoLess);
                    player.spawnParticle(Particle.SMOKE_LARGE, playerLocation, 5);
                }
            }
        }
    }
}