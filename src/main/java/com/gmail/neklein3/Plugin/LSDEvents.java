package com.gmail.neklein3.Plugin;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;

public class LSDEvents implements Listener {

    @EventHandler
    public void blockBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        Player player = event.getPlayer();

        ItemStack lsdExtract = new ItemStack(Material.IRON_NUGGET, 1);
        ItemMeta meta = lsdExtract.getItemMeta();

        if (block.getType().equals(Material.WHEAT)) {
            meta.setDisplayName(ChatColor.GRAY + "LSD Extract");
            lsdExtract.setItemMeta(meta);
            player.getInventory().addItem(lsdExtract);
        }
    }

    @EventHandler
    public void onPlayerUseLSDExtract(PlayerInteractEvent event)  {
        Player player = event.getPlayer();
        Action action = event.getAction();
        ItemStack item = player.getInventory().getItemInMainHand();

        if (action.equals(Action.RIGHT_CLICK_BLOCK)) {
            if (item.getType().equals(Material.IRON_NUGGET))  {
                if (item.getItemMeta().getDisplayName().equals(ChatColor.GRAY + "LSD Extract")) {
                    player.sendMessage(ChatColor.RED + "combine this with paper to make usable LSD");
                }
            }
        } else if (action.equals(Action.RIGHT_CLICK_AIR)) {
            if (item.getType().equals(Material.IRON_NUGGET))  {
                if (item.getItemMeta().getDisplayName().equals(ChatColor.GRAY + "LSD Extract")) {
                    player.sendMessage(ChatColor.RED + "combine this with paper to make usable LSD");
                }
            }
        }
    }

    @EventHandler
    public void onPlayerUseLSD(PlayerInteractEvent event)  {
        Player player = event.getPlayer();
        Action action = event.getAction();
        ItemStack item = player.getInventory().getItemInMainHand();

        int originalItemAmount = item.getAmount();
        ItemStack itemOneLess = new ItemStack(Material.PAPER, originalItemAmount - 1);
        ItemStack itemTwoLess = new ItemStack(Material.PAPER, originalItemAmount - 2);
        ItemMeta meta = itemOneLess.getItemMeta();
        ItemMeta meta2 = itemTwoLess.getItemMeta();

        meta.setDisplayName(ChatColor.GRAY + "LSD");
        meta2.setDisplayName(ChatColor.GRAY + "LSD");

        itemOneLess.setItemMeta(meta);
        itemTwoLess.setItemMeta(meta);

        Location playerLocation = player.getLocation();

        if (action.equals(Action.RIGHT_CLICK_BLOCK)) {
            if (item.getType().equals(Material.PAPER))  {
                if (item.getItemMeta().getDisplayName().equals(ChatColor.GRAY + "LSD")) {
                    player.removePotionEffect(PotionEffectType.SPEED);
                    player.removePotionEffect(PotionEffectType.CONFUSION);
                    player.removePotionEffect(PotionEffectType.JUMP);
                    player.removePotionEffect(PotionEffectType.SLOW);
                    player.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 600, 7));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 1000, 1));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 500, 1));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 400, 2));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 1000, 2));
                    player.getInventory().removeItem(item);
                    player.getInventory().addItem(itemOneLess);
                    player.spawnParticle(Particle.BUBBLE_COLUMN_UP, playerLocation, 100);
                }
            }
        } else if (action.equals(Action.RIGHT_CLICK_AIR)) {
            if (item.getType().equals(Material.PAPER))  {
                if (item.getItemMeta().getDisplayName().equals(ChatColor.GRAY + "LSD")) {
                    player.removePotionEffect(PotionEffectType.SPEED);
                    player.removePotionEffect(PotionEffectType.CONFUSION);
                    player.removePotionEffect(PotionEffectType.JUMP);
                    player.removePotionEffect(PotionEffectType.SLOW);
                    player.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 600, 7));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 1000, 1));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 500, 1));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 400, 2));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 1000, 2));
                    player.getInventory().removeItem(item);
                    player.getInventory().addItem(itemTwoLess);
                    player.spawnParticle(Particle.BUBBLE_COLUMN_UP, playerLocation, 100);
                }
            }
        }
    }

}